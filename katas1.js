function oneThroughTwenty() {
    const numbers = [];

    // Your code goes below
    for(let counter = 0; counter <= 20; counter+=1) { 
      numbers.push(counter) }

    
    return numbers;
}

function evensToTwenty() {
    const numbers = [];
    
    // Your code goes below
    for(let counter= 0; counter <= 20; counter+=1) { 
        if(counter %2===0){
        numbers.push(counter) }
}

    return numbers;
}

function oddsToTwenty() {
    const numbers = [];
    
    // Your code goes below
    for(let counter= 0; counter <= 20; counter+=1) { 
        if(counter%2 !==0) {
        numbers.push(counter) }
}


    return numbers;
}

function multiplesOfFive() {
    const numbers = [];
    
    // Your code goes below
    for(let counter = 5; counter <= 100; counter+=5) { 
      numbers.push(counter) }



    return numbers;
}

function squareNumbers() {
    const numbers = [];
    
    // Your code goes below
    for(let counter =0; counter <= 10; counter++) {
        square= counter*counter;
        numbers.push(square) }
    
    return numbers
}

function countingBackwards() {
    const numbers = [];
    
    // Your code goes below
    for(let counter = 20; counter >= 0; counter-=1) { numbers.push(counter) }
    return numbers;
}

function evenNumbersBackwards() {
    const numbers = [];
    
    // Your code goes below
    for(let counter = 20; counter >= 0; counter-=2) { numbers.push(counter) }
    return numbers;
}

function oddNumbersBackwards() {
    const numbers = [];
    
    // Your code goes below
    for(let counter = 20; counter >= 3; counter-=1) { numbers.push(counter) }
    return numbers;
}

function multiplesOfFiveBackwards() {
    const numbers = [];
    
    // Your code goes below
    for(let counter = 100; counter >=5; counter-=5) { numbers.push(counter) }
    return numbers;
}

function squareNumbersBackwards() {
    const numbers = [];
    // Your code goes below
    for(let counter =10; counter >= 0; counter-=1) {
        square= counter*counter;
        numbers.push(square) }
    

    return numbers;
}
